@extends("root")

@section("content")
<div class="row quiz-container">
	@if($results)
		<div class="col-md-12 text-center">
			<h3>Twój wynik - {{ $quiz->percentage_result }}</h3>
			<h4>Poprawnych odpowiedzi {{ $quiz->correct_answers }}/{{ $quiz->questions->count() }}</h4>
			<h5><small>Poniżej możesz sprawdzić poprawne odpowiedzi, wystawić komentarz i ocenić quiz</small></h5>
			<hr>
		</div>
	@endif
	<div class="col-md-8">
		
		@if(!$results)
			<div class="quiz-card-image" style="background-image:url({{ URL::asset($quiz->getImage()) }})"></div>
			<hr>
			<span class="pull-right text-right">
				<small>
					średnia ocen: {{ $quiz->getAvgRating() }}/5
					<br>
					twórca: <strong>~{{ $quiz->user->name }}</strong>
				</small>
			</span>
			<h4>{{ $quiz->name }}</h4>
			<p>{{ $quiz->description }}</p>
			<hr>
		@endif
		<form action="{{ route("results.post") }}" method="post">
			{{ csrf_field() }}
			<input type="hidden" name="quiz" value="{{ $quiz->id }}">
			@foreach($quiz->questions as $index => $question)
				@if($results)
					@include("quiz.question-answered")
				@else
					@include("quiz.question")
				@endif
			@endforeach

			@if(!$results)
				<button type="submit" class="btn btn-success submit-btn btn-lg pull-right">Sprawdź wynik!</button>
			@endif
		</form>
		@include("quiz.rating")
	</div>
	<div class="col-md-4">
		@include("quiz.sidebar")
		@if($results)
		@endif
	</div>
	@if($results)
		<div class="col-md-12">
			<a href="{{ route("quizzes.show", $quiz->id) }}" class="btn btn-info pull-right start-again-btn" role="button">
				Rozpocznij jeszcze raz!
			</a>
		</div>
	@endif
	@if($quiz->canEdit())
		<div class="col-md-12">
			<a href="{{ route("quizzes.edit", $quiz->id) }}" class="btn btn-info pull-right start-again-btn" role="button">
				Edytuj quiz
			</a>
		</div>
	@endif
</div>
@endsection

@section("styles")
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/quiz.css') }}">
@endsection