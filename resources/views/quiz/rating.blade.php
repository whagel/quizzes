@if(Auth::check())
<h5>Oceń quiz</h5>
<div class="stars">	
	<form method="post" action="{{ route('ratings.store') }}">
		{{ csrf_field() }}
		<input type="hidden" name="quiz_id" value="{{$quiz->id}}">
		<input class="star star-5" id="star-5" type="radio" name="value" value="5"/>
		<label class="star star-5" for="star-5"></label>
		<input class="star star-4" id="star-4" type="radio" name="value" value="4"/>
		<label class="star star-4" for="star-4"></label>
		<input class="star star-3" id="star-3" type="radio" name="value" value="3"/>
		<label class="star star-3" for="star-3"></label>
		<input class="star star-2" id="star-2" type="radio" name="value" value="2"/>
		<label class="star star-2" for="star-2"></label>
		<input class="star star-1" id="star-1" type="radio" name="value" value="1"/>
		<label class="star star-1" for="star-1"></label>
		<button type="submit" class="btn btn-info submit-btn">Prześlij ocenę</button>	
	</form>
</div>
@endif
