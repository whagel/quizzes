<div class="question-container"> 
	<h3 class="text-center">{{ $index+1 }}. {{ $question->content }}</h3>
	<h4 class="text-center">
		<small>
			{{ $question->getCorrectAnswers() }}
		</small>
	</h4>
	<div class="col-md-12 quiz-card">
		<div class="btn-group btn-group-vertical question-answers" data-toggle="buttons">
			@foreach($question->answers as $answer)
			<label class="btn btn-primary well">
				<input type="checkbox" name="{{ $question->id }}[]" value="{{ $answer->id }}">
				<div class="answer-content">{{ $answer->content }}</div>
			</label>
			@endforeach
		</div>
	</div>
</div>
<hr>