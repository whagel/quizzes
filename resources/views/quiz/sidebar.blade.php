
<ul class="nav nav-tabs" role="tablist">
	<li class="nav-item">
		<a class="nav-link active" data-toggle="tab" href="#home" role="tab">Komentarze</a>
	</li>

</ul>

@if(Auth::check())
<div>
	<form class="form-horizontal" action="{{ route('comments.store') }}" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="quiz_id" value="{{ $quiz->id }}">
		<textarea id="comment" name="content" placeholder="Treść komentarza..." class="form-control input-md" rows="3"  required></textarea>
		<button type="submit" class="btn btn-success pull-right">Dodaj komentarz</button>
	</form>
</div>
<br>
@endif
<hr>
<div class="tab-content">

	@foreach($quiz->comments->sortByDesc("created_at") as $comment)
	<div>								
		{{ $comment->content }}	
	</div>
	<div>
		<small><strong>~{{ $comment->user->name }}</small></strong>
		<i class="pull-right"><small>{{ $comment->created_at->diffForHumans() }}</small></i>
	</div>
	<hr>
	@endforeach
</div>

