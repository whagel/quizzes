<div class="question-container {{ $question->class }}"> 
	<h3 class="text-center">{{ $index+1 }}. {{ $question->content }}</h3>
	<div class="col-md-12 quiz-card">
		<div class="btn-group btn-group-vertical question-answers" data-toggle="buttons">
			@foreach($question->answers as $answer)
			<label class="btn btn-secondary disabled well {{ $answer->class }}">
			{{ $answer->content }}
		</label>
		@endforeach
	</div>

</div>
</div>
<hr>