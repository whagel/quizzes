@extends("root")

@section("content")
<div class="quiz-container">
	<div class="row">
		<div class="col-md-12">
			<h4 class="text-center">Logowanie</h4>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<form class="form-horizontal" action="{{ route('login.post') }}" method="post">
				{{ csrf_field() }}
				<fieldset>
					<div class="form-group">
						<label class="col-md-12 control-label" for="email">E-mail</label>  
						<div class="col-md-12">
							<input id="email" name="email" type="email" placeholder="Podaj e-mail" class="form-control input-md" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12 control-label" for="password">Hasło</label>
						<div class="col-md-12">
							<input id="password" name="password" type="password" placeholder="Podaj hasło" class="form-control input-md" required>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="col-md-12">
							<button type="submit" class="btn btn-success">Zaloguj</button>
						</div>
					</div>
					<hr>
					<p class="text-center">
						Nie masz jeszcze konta? <a href="{{ route('register.get') }}">Zarejestruj się!</a>
					</p>
				</fieldset>
			</form>
		</div>
	</div>
</div>

@endsection
