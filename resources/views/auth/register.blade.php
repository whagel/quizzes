@extends("root")

@section("content")
<div class="quiz-container">
	<div class="row">
		<div class="col-md-12">
			<h4 class="text-center">Rejestracja</h4>
			<hr>
		</div>
	</div>
	<div class="row">
		<div class="col-md-6 offset-md-3">
			<form class="form-horizontal" action="{{ route('register.post') }}" method="post">
				{{ csrf_field() }}
				<fieldset>
					<div class="form-group">
						<label class="col-md-12 control-label">Nazwa użytkownika</label>  
						<div class="col-md-12">
							<input id="name" name="name" type="text" placeholder="Podaj nazwę użytkownika" class="form-control input-md" value="{{ old('name') }}" required>
						</div>
					</div>
					<div class="form-group">
						<label class="col-md-12 control-label">E-mail</label>  
						<div class="col-md-12">
							<input id="email" name="email" type="email" placeholder="Podaj e-mail" class="form-control input-md" value="{{ old('email') }}" required>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<label class="col-md-12 control-label">Hasło</label>
						<div class="col-md-12">
							<input id="password" name="password" type="password" placeholder="Podaj hasło" class="form-control input-md" required>
						</div>
					</div>

					<div class="form-group">
						<label class="col-md-12 control-label">Powtórz hasło</label>
						<div class="col-md-12">
							<input id="password_confirmation" name="password_confirmation" type="password" placeholder="Powtórz hasło" class="form-control input-md" required>
						</div>
					</div>
					<hr>
					<div class="form-group">
						<div class="col-md-12">
							<button id="" name="" class="btn btn-success">Rejestracja</button>
						</div>
					</div>
					<hr>
					<p class="text-center">
						Masz już konto? <a href="{{ route('login') }}">Zaloguj się!</a>
					</p>
				</fieldset>
			</form>
		</div>
	</div>
</div>

@endsection
