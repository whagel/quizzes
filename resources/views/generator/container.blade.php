@extends("root")

@section("content")

<div class="row quiz-container">
	<div class="col-md-12">
		@if(isset($quiz))
		<h4 class="text-center">Edytuj quiz</h4>
		@else
		<h4 class="text-center">Generator quizów <small> - stwórz nowy quiz!</small></h4>
		@endif
		<hr>
		<form class="form-horizontal" method="POST" id="quiz-form" 
			action="@if(isset($quiz)){{ route('quizzes.update', $quiz->id) }}@else{{ route('quizzes.store') }}@endif">

			{{ csrf_field() }}
			<fieldset>
				<div class="form-group">
					<label class="col-md-4 control-label" for="title">Obrazek quizu</label>  
					<div class="col-md-8">
						<input type="file" name="quiz_image" id="quiz-image">
					</div>
				</div>
				<div class="form-group">
					<label class="col-md-4 control-label" for="title">Tytuł quizu</label>  
					<div class="col-md-8">
						<input id="quiz-name" name="name" type="text" placeholder="Tu zamieść tytuł swojego quizu..." class="form-control input-md" 
						@if(isset($quiz)) value="{{ $quiz->name }}" @endif
						required>
					</div>
				</div>

				<div class="form-group">
					<label class="col-md-4 control-label" for="description">Opis</label>
					<div class="col-md-8">                     
						<textarea class="form-control" id="quiz-description" name="description" 
						placeholder="Tu zamieść opis swojego quizu..." required>@if(isset($quiz)){{ $quiz->description }}@endif</textarea>
					</div>
				</div>

				<hr>

				<h5>Pytania:</h5>
				
				<div id="question-container">
					@if(isset($quiz))
					@foreach($quiz->questions as $question)
					@include("generator.question")
					@endforeach
					@endif
				</div>

				<hr>

				<button type="button" class="btn btn-primary submit-btn" id="add-question">
					<i class="fa fa-plus" aria-hidden="true"></i>
					Dodaj pytanie
				</button>

				<button type="submit" class="btn btn-success submit-btn btn-lg pull-right" id="publish-quiz">
					Opublikuj quiz
				</button>

			</fieldset>
		</form>
	</div>
</div>

<div id="question-template" style="display:none;">
	@include("generator.question", ["question" => null])
</div>
<div id="answer-template" style="display:none;">
	@include("generator.answer")
</div>

<input id="is-edited" type="hidden" value="{{ isset($quiz) }}">

@endsection

@section("styles")
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/generator.css') }}">
@endsection

@section("scripts")
<script src="{{ URL::asset('js/generator/generator.js') }}"></script>
<script src="{{ URL::asset('js/generator/quiz-publish.js') }}"></script>
@endsection



