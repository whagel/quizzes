<div class="card quiz-question-card col-md-8">
	<div class="card-header">
		<span>
			<h6 id="question-title"></h6>
		</span>
		<span>
			<button type="button" class="close close-question-card">
				<span aria-hidden="true">&times;</span>
			</button>
		</span>
	</div>
	<div class="card-block">
		<fieldset>
			<div class="form-group">
				<div class="col-md-12">
					<input id="question" name="question" type="text" placeholder="Pytanie..." class="form-control input-md" required @if(isset($question)) value="{{ $question->content }}" @endif>
				</div>
			</div>
			<hr>
			<div class="form-group">
				<label class="col-md-4 control-label" for="title">
					<small>Odpowiedzi:</small>
				</label>  
				<div class="col-md-12">

					<div class="answer-container">
						@if(isset($question))
						@foreach($question->answers as $answer) 
							@include("generator.answer")
						@endforeach
						@endif
					</div>

					<hr>

					<button type="button" class="btn btn-primary btn-sm add-answer">
						<i class="fa fa-plus" aria-hidden="true"></i>
						Dodaj odpowiedź
					</button>

				</div>
			</div>
		</fieldset>
	</div>
</div>
