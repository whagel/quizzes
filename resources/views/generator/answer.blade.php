<div class="answer">
	<input id="name" name="answer" type="text" placeholder="Tekst odpowiedzi" class="form-control input-md answer" required
	@if(isset($answer)) value="{{ $answer->content }}" @endif>
	<label class="form-check-label correct-answer">
		<input class="form-check-input correct-answer-checkbox" type="checkbox" value="option1"
		@if(isset($answer) && $answer->is_correct) checked @endif> Poprawna odpowiedź
	</label>
	<button type="button" class="close remove-answer">
		<span aria-hidden="true">&times;</span>
	</button>
</div>