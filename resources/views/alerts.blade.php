@foreach($errors->all() as $error)
	<div class="alert alert-danger" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Błąd!</strong> {{ $error }}
	</div>
@endforeach

@if(Session::has("success"))
	<div class="alert alert-success" role="alert">
		<button type="button" class="close" data-dismiss="alert" aria-label="Close">
			<span aria-hidden="true">&times;</span>
		</button>
		<strong>Sukces!</strong> {{ Session::get("success") }}
	</div>
@endif