@extends("root")

@section("content")

<h5 class="section-name hidden-sm-down">Top 5 quizów</h5>
<div class="row">
	<div class="col-md-12">
		<div id="bestQuizzesCarousel" class="carousel slide quiz-carousel hidden-sm-down" data-ride="carousel">
			<div class="carousel-inner" role="listbox">
				@foreach($top_5_quizzes->values() as $key => $quiz)
				<div class="carousel-item @if($key == 0) active @endif">
					<img class="d-block img-fluid" src="{{ URL::asset($quiz->getImage()) }}" >
					<a href="{{ route('quizzes.show', $quiz->id) }}">
						<div class="carousel-caption">

							<h3>{{ $quiz->name }}</h3>
							<p>{{ $quiz->description }}</p>

						</div>
					</a>
				</div>
				@endforeach
			</div>
			<a class="carousel-control-prev" href="#bestQuizzesCarousel" role="button" data-slide="prev">
				<span class="carousel-control-prev-icon" aria-hidden="true"></span>
				<span class="sr-only">Previous</span>
			</a>
			<a class="carousel-control-next" href="#bestQuizzesCarousel" role="button" data-slide="next">
				<span class="carousel-control-next-icon" aria-hidden="true"></span>
				<span class="sr-only">Next</span>
			</a>
		</div>
	</div>
</div>

<h5 class="section-name">Najlepsze z ostatniego miesiąca</h5>
<div class="row">
	@include("quiz-cards")
</div>
@endsection
