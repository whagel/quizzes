<div class="col-md-3 admin-panel sidebar">
	<ul class="list-unstyled">
		@foreach($admin_table->getSidebarElements() as $sidebar_element)
		<a href="{{ route($sidebar_element->getRoute()) }}">
			<li>
				<i class="fa {{ $sidebar_element->getIcon() }}" aria-hidden="true"></i> 
				{{ $sidebar_element->getName() }}
			</li>
		</a>
		@endforeach
	</ul>
</div>