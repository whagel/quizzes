<div class="modal fade" id="{{ $action->getModalName($row->id) }}" tabindex="-1" role="dialog" " aria-hidden="true">
	<div class="modal-dialog" role="document">
		<div class="modal-content">
			<div class="modal-header">
				<h5 class="modal-title">{{ $action->getConfirmationModal()->getModalTitle() }}</h5>
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">&times;</span>
				</button>
			</div>
			<div class="modal-body">
				@foreach($row->getAttributes() as $key => $attribute)
					<div>
						<strong>{{ $key }}:</strong>
						{{ str_limit($attribute, 25, "...") }}
					</div>
				@endforeach
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-secondary" data-dismiss="modal">Zamknij</button>
				<button type="submit" class="btn btn-success">Tak</button>
			</div>
		</div>
	</div>
</div>