@extends("root")

@section("content")

<div class="row">
	<div class="col-md-12 admin-header">
		<h3 class="section-name">Panel administracyjny - {{ $admin_table->getTitle() }}</h3>
	</div>
</div>
<div class="row">
	@include("admin.sidebar")
	<div class="col-md-9 admin-panel">
		<div class="table-responsive">
			<table class="table table-striped table-hover">
				<thead>
					<tr>
						@foreach($admin_table->getColumns() as $column)
						<th>{{ $column->getLabel() }}</th>
						@endforeach
						@if($admin_table->hasActions())
						<th colspan="{{ sizeof($admin_table->getActions()) }}">Akcje</th>
						@endif
					</tr>
				</thead>
				<tbody>
					@foreach($admin_table->getData() as $row)
					<tr>
						@foreach($row->getAttributes() as $attribute)
						<td>{{ str_limit($attribute, 50, "...") }}</td>
						@endforeach
						@foreach($admin_table->getActions() as $action)
						<td class="action">
							@include("admin.action")
						</td>
						@endforeach
					</tr>
					@endforeach
				</tbody>
			</table>
		</div>
		<div class="paginator-container">
			{{ $admin_table->getData()->links() }}
		</div>
		@if(sizeof($admin_table->getData()) == 0)<h4>Brak zawartości</h4>@endif
	</div>
</div>


@endsection

@section("styles")
<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/admin.css') }}">
@endsection
