@if($action->isPostAction())
	<form class="form-horizontal" action="{{ route($action->getRoute(), $row->id) }}" method="post">
		{{ csrf_field() }}
		<input type="hidden" name="id" value="{{ $row->id }}">
		<button class="btn btn-{{ $action->getBtnStyle() }}" @if($action->isConfirmationModalSet()) type="button" data-toggle="modal" data-target="#{{ $action->getModalName($row->id) }}"" @else type="submit" @endif>
			<i class="fa {{ $action->getBtnIcon() }}" aria-hidden="true"></i> 
		</button>
		@if($action->isConfirmationModalSet()) 
		@include("admin.confirmation-modal")
		@endif
	</form>
@else
	<a class="btn btn-{{ $action->getBtnStyle() }}" 
		href="{{ route($action->getRoute(), $row->id)}}" role="button">
		<i class="fa {{ $action->getBtnIcon() }}" aria-hidden="true"></i> 
	</a>
@endif