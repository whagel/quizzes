<div class="row">
	@foreach($quizzes as $quiz)
	<div class="col-md-6 quiz-card">
		<div class="quiz-card-image" style="background-image:url('{{ URL::asset($quiz->getImage()) }}')">
		</div>
		<div class="quiz-card-body">
			<span><h2>{{ $quiz->name }}</h2></span>
			<p>{{ $quiz->description }}</p>
			<span>
				<a class="btn btn-success" href="{{ route('quizzes.show', $quiz->id) }}" role="button">Wypełnij quiz! &raquo;</a>
			</span>
			<span class="pull-right text-right">
				<small>
					średnia ocen: {{ $quiz->getAvgRating() }}/5
					<br>
					twórca: <strong>~{{ $quiz->user->name }}</strong>
				</small>
			</span>
		</div>
	</div>
	@endforeach
</div>

