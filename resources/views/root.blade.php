<!DOCTYPE html>
<html lang="pl">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<meta name="description" content="">
	<meta name="author" content="">
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Quizy</title>
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/css/bootstrap.min.css" integrity="sha384-rwoIResjU2yc3z8GV/NPeZWAv56rSmLldC3R/AZzGRnGxQQKnKkoFVhFQhNUwEyJ" crossorigin="anonymous">
	<link rel="stylesheet" href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css">
	
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('css/styles.css') }}">
	
	<script async src="https://www.googletagmanager.com/gtag/js?id=UA-112771908-1"></script>
	<script>
		window.dataLayer = window.dataLayer || [];
		function gtag(){dataLayer.push(arguments);}
		gtag('js', new Date());
		gtag('config', 'UA-112771908-1');
	</script>

	
	@yield("styles")
</head>

<body>

	<nav class="navbar navbar-toggleable-md navbar-inverse fixed-top bg-inverse">
		<button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#main-navbar" aria-controls="main-navbar" aria-expanded="false" aria-label="Toggle navigation">
			<span class="navbar-toggler-icon"></span>
		</button>

		<a class="navbar-brand" href="{{ route('index') }}">
			<img src="{{ URL::asset('logo.png') }}">
		</a>

		<div class="collapse navbar-collapse" id="main-navbar">
			<ul class="navbar-nav mr-auto">
				<li class="nav-item">
					<a class="nav-link" href="{{ route('quizzes.top') }}">Top quizy</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('quizzes.newest') }}">Najnowsze</a>
				</li>
				<li class="nav-item">
					<a class="nav-link" href="{{ route('quizzes.create') }}">Dodaj quiz</a>
				</li>
				@if(Auth::check() && Auth::user()->isAdmin())
				<li class="nav-item">
					<a class="nav-link" href="{{ route('admin.panel') }}">Panel administracyjny</a>
				</li>
				@endif
			</ul>
			
			<ul class="navbar-nav right-navbar">
				
				<li class="nav-item active">				
					@if(Auth::check())
					<li class="nav-item">
						<a class="nav-link">
							<small>Zalogowany jako <strong>{{ Auth::user()->name }}</strong></small>
						</a>
					</li>
					<li class="nav-item">
						<form action="{{ route('logout') }}" method="post">
							{{ csrf_field() }}
							<button class="nav-link btn btn-link" type="submit">Wyloguj</button>
						</form>
					</li>
					@else
					<a class="nav-link" href="{{route('login')}}">Zaloguj się</a>
					<li class="nav-item active">				

						<a class="nav-link" href="{{route('register.get') }}">Rejestracja</a>

					</li>
					@endif
				</li>
			</ul>						
			
		</div>
	</nav>

	<div class="container">
		@include("alerts")
		@yield("content")
		<hr>
		<footer>
			<p>&copy; Quizzes 2018</p>
		</footer>
	</div>

	<script
	src="https://code.jquery.com/jquery-3.2.1.min.js"
	integrity="sha256-hwg4gsxgFZhOsEEamdOYGBf13FyQuiTwlAQgxVSNgt4="
	crossorigin="anonymous"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0-alpha.6/js/bootstrap.min.js" integrity="sha384-vBWWzlZJ8ea9aCX4pEW3rVHjgjt7zpkNpZk+02D9phzyeVkE+jo0ieGizqPLForn" crossorigin="anonymous"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
	@yield("scripts")
</body>
</html>
