<?php

Route::get("/", ["uses" => "QuizController@getIndexView"])->name("index");
Route::get("/top", ["uses" => "QuizController@getTopQuizzesView"])->name("quizzes.top");
Route::get("/newest", ["uses" => "QuizController@getNewestQuizzesView"])->name("quizzes.newest");
Route::get("/quizzes/{id}", ["uses" => "QuizController@getQuizView"])->where("id", "[0-9]+")->name("quizzes.show");
Route::post("/results", ["uses" => "QuizController@getQuizResults"])->name("results.post");

Route::group(["middleware" => "guest"], function () {
	Route::get("/login", ["uses" => "AuthController@getLoginView"])->name("login");
	Route::get("/register", ["uses" => "AuthController@getRegisterView"])->name("register.get");
	Route::post("/login", ["uses" => "AuthController@login"])->name("login.post");
	Route::post("/register", ["uses" => "AuthController@register"])->name("register.post");
});

Route::group(["middleware" => ["auth", "not_banned"]], function () {
	Route::post("/logout", ["uses" => "AuthController@logout"])->name("logout");
	Route::get("/quizzes/create", ["uses" => "QuizController@getQuizGeneratorView"])->name("quizzes.create");
	Route::get("/quizzes/{id}/edit", ["uses" => "QuizController@getEditQuizView"])->where("id", "[0-9]+")->name("quizzes.edit");
	Route::post("/quizzes/{id}", ["uses" => "QuizController@updateQuiz"])->name("quizzes.update");
	Route::post("/ratings", ["uses" => "RatingController@storeRating"])->name("ratings.store");
	Route::post("/quizzes", ["uses" => "QuizController@storeQuiz"])->name("quizzes.store");
	Route::post("/comments", ["uses" => "CommentController@storeComment"])->name("comments.store");
});

Route::group(["middleware" => "auth"], function () {
	Route::post("/logout", ["uses" => "AuthController@logout"])->name("logout");
});

Route::group(["middleware" => ["admin", "not_banned"]], function () {
	Route::get("/admin/panel", ["uses" => "AdminPanelController@getAdminPanelView"])->name("admin.panel");
	Route::get("/admin/questions", ["uses" => "AdminPanelController@getQuestionsTableView"])->name("admin.questions");
	Route::get("/admin/answers", ["uses" => "AdminPanelController@getAnswersTableView"])->name("admin.answers");
	Route::get("/admin/users", ["uses" => "AdminPanelController@getUsersTableView"])->name("admin.users");
	Route::get("/admin/comments", ["uses" => "AdminPanelController@getCommentsTableView"])->name("admin.comments");
	Route::get("/admin/ratings", ["uses" => "AdminPanelController@getRatingsTableView"])->name("admin.ratings");
	Route::get("/admin/admins", ["uses" => "AdminPanelController@getAdminsTableView"])->name("admin.admins");
	Route::get("/admin/bans", ["uses" => "AdminPanelController@getBansTableView"])->name("admin.bans");
	
	Route::post("/quizzes/delete/{id}", ["uses" => "QuizController@deleteQuiz"])->name("quizzes.destroy");
	Route::post("/comments/delete/{id}", ["uses" => "CommentController@deleteComment"])->name("comments.destroy");
	Route::post("/comments/ratings/{id}", ["uses" => "RatingController@deleteRating"])->name("ratings.destroy");
	Route::post("/bans/delete/{id}", ["uses" => "UserBanController@deleteBan"])->name("bans.destroy");
	Route::post("/users/ban/{id}", ["uses" => "UserController@banUser"])->name("user.ban");
});

