<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizImagesTable extends Migration {

	public function up() {
		Schema::create("quiz_images", function (Blueprint $table) {
			$table->increments("id");
			$table->integer("quiz_id");
			$table->string("path");
			$table->timestamps();
		});
	}

	public function down() {
		Schema::dropIfExists("quiz_images");
	}
}
