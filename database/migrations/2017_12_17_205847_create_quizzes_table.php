<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateQuizzesTable extends Migration {

	public function up() {
		Schema::create("quizzes", function (Blueprint $table) {
			$table->increments("id");
			$table->integer("user_id");
			$table->string("name");
			$table->text("description");
			$table->timestamps();
		});
	}

	public function down() {
		Schema::dropIfExists("quizzes");
	}
}
