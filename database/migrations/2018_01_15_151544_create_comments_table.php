<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateCommentsTable extends Migration
{
    public function up() {
        Schema::create("comments", function (Blueprint $table) {
            $table->increments("id");
            $table->integer("quiz_id");
            $table->integer("user_id");
            $table->string("content");
            $table->timestamps();
        });
    }


    public function down()
    {
        Schema::dropIfExists('comments');
    }
}
