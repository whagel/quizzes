<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

use App\Models\Comment;
use App\Models\Quiz;
use App\Models\User;


class CommentsTableSeeder extends Seeder {    

    public function run() {
		$faker = Factory::create();

		foreach(Quiz::all() as $quiz) {
			for($i = 0; $i < rand(8, 15); $i++) {
				$comment = new Comment();
				$comment->content = $faker->sentence(rand(2, 6), true);
				$comment->user_id = User::inRandomOrder()->first()->id;
				$quiz->questions()->save($comment);
			}
		}
	}

}
