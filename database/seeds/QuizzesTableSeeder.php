<?php

use Illuminate\Database\Seeder;
use Faker\Factory;
use Carbon\Carbon;

use App\Models\User;

class QuizzesTableSeeder extends Seeder {

	public function run() {
		$faker = Factory::create();

		for($i = 0; $i < 25; $i++) {
			DB::table("quizzes")->insert([
				"user_id" => User::inRandomOrder()->first()->id,
				"name" => $faker->sentence(rand(1, 3), true),
				"description" => $faker->sentence(rand(10, 20), true),
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now()
			]);
		}
	}
}
