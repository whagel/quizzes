<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

use App\Models\Quiz;
use App\Models\Question;

class QuestionsTableSeeder extends Seeder {

	public function run() {
		$faker = Factory::create();

		foreach(Quiz::all() as $quiz) {
			for($i = 0; $i < rand(8, 15); $i++) {
				$question = new Question();
				$question->content = $faker->sentence(rand(2, 6), true);
				$quiz->questions()->save($question);
			}
		}
	}
}
