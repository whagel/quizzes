<?php

use Illuminate\Database\Seeder;

use App\Models\Quiz;
use App\Models\Rating;
use App\Models\User;

class RatingsTableSeeder extends Seeder {

	public function run() {
		foreach(Quiz::all() as $quiz) {
			for($i = 0; $i < rand(8, 15); $i++) {
				$rating = new Rating();
				$rating->user_id = User::inRandomOrder()->first()->id;
				$rating->value = rand(1, 5);
				$quiz->questions()->save($rating);
			}
		}
	}
}
