<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

use App\Models\User;
use App\Models\Admin;

class UsersTableSeeder extends Seeder {
    
    public function run() {
		$faker = Factory::create();

		for($i = 0; $i < 10; $i++) {
			$user = new User();
			$user->name = $faker->lastName();
			$user->email = $faker->safeEmail();
			$user->password = Hash::make("password");
			$user->save();
		}

		$user = new User();
		$user->name = "Admin";
		$user->email = "admin@example.com";
		$user->password = Hash::make("password");
		$user->save();

		$admin = new Admin();
		$user->admin()->save($admin);
	}
}
