<?php

use Illuminate\Database\Seeder;
use Faker\Factory;

use App\Models\Question;
use App\Models\Answer;


class AnswersTableSeeder extends Seeder {

	public function run() {
		$faker = Factory::create();

		foreach(Question::all() as $question) {
			for($i = 0; $i < rand(2, 6); $i++) {
				$answer = new Answer();
				$answer->content = $faker->sentence(rand(2, 6), true);
				$answer->is_correct = rand(0, 1);
				$question->answers()->save($answer);
			}
		}
	}
}
