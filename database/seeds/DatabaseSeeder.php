<?php

use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder {
	
	public function run() {
		$this->call([
			UsersTableSeeder::class,
			QuizzesTableSeeder::class,
			QuestionsTableSeeder::class,
			AnswersTableSeeder::class,
			CommentsTableSeeder::class,
			RatingsTableSeeder::class,
		]);
	}
}
