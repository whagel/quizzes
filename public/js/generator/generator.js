var questionTemplate = $("#question-template").html();
var answerTemplate = $("#answer-template").html();

if(!$( "#is-edited" ).val() == 1) {
	appendNewQuestion()
}

reorderQuestionCardTitles();

$( "#add-question" ).click(function() {
	appendNewQuestion();
});

$( "#question-container" ).on( "click", ".close-question-card", function() {
	if(onlyOneQuestionExists()) {
		return;
	}

	var questionCard = $(this).closest(".quiz-question-card");
	questionCard.remove();

	reorderQuestionCardTitles();
});

$( "#question-container" ).on( "click", ".remove-answer", function() {

	var answersAmount = $(this).parent().parent().find("div.answer").length;
	if(answersAmount > 2) {
		$(this).parent().remove();
	}
});


$( "#question-container" ).on( "click", ".add-answer", function() {
	var answerContainer = $(this).parent().children(".answer-container");
	appendNewAnswerTo(answerContainer);
});

function appendNewQuestion() {
	var question = $(questionTemplate);
	$("#question-container").append(question);

	var answerContainer = question.find(".answer-container");
	appendNewAnswerTo(answerContainer);
	appendNewAnswerTo(answerContainer);

	reorderQuestionCardTitles();
}

function appendNewAnswerTo(answerContainer) {
	answerContainer.append(answerTemplate);
}

function onlyOneQuestionExists() {
	return getQuestionsAmount() <= 1;
}

function getQuestionsAmount() {
	return $(".quiz-question-card").length - 1;
}

function reorderQuestionCardTitles() {
	$(".quiz-question-card").each(function( index ) {
		$(this).find("#question-title").text( "Pytanie " + (index + 1) + ".");
	});
}