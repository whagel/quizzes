$.ajaxSetup({
	headers: {
		"X-CSRF-TOKEN": $("meta[name='csrf-token']").attr("content")
	}
});

var quizData = new FormData();

$("#quiz-form").on("submit",(function(e) {	
	e.preventDefault();

	quizData.append("name", $("#quiz-name").val());
	quizData.append("description", $("#quiz-description").val());
	
	if($("#quiz-image").prop("files").length != 0) {
		var quizImage = $("#quiz-image").prop("files")[0];
		quizData.append("quiz_image", quizImage);
	}

	quizData.append("questions", getQuestions())

	var url = $(this).attr("action");
	sendAjaxRequest(url);
}));

function sendAjaxRequest(url) {
	$.ajax({
		type: "POST",
		url: url,
		contentType: false,
		processData: false,
		data: quizData,
		success: function (id) {
			window.location = "/quizzes/"+id;
		},
		error: function (response) {
			printErrorMessages(response.responseJSON.errors)
		}
	});
}

function getQuestions() {
	var questions = [];

	$(".quiz-question-card").not("#question-template>.quiz-question-card").each(function( index ) {

		var question = {};
		question.answers = [];
		question.content = $(this).find("#question").val();

		var answerContainer = $(this).find(".answer-container");

		answerContainer.find("input.answer").each(function( index ) {
			var answer = {};
			answer.content = $(this).val();
			answer.is_correct = $(this).parent().find(".correct-answer-checkbox").is(":checked");
			question.answers.push(answer)
		});

		questions.push(question);
	});

	return JSON.stringify(questions);
}

function printErrorMessages(errors) {
	for (var key in errors) {
		for (let error of errors[key]) {
			toastr.error(error);
		}
	}
}