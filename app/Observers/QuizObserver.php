<?php

namespace App\Observers;

use App\Models\Quiz;

class QuizObserver {
	public function deleting(Quiz $quiz) {
		$quiz->image()->delete();
		$quiz->questions()->delete();
	}
}