<?php

namespace App\Observers;

use App\Models\Question;

class QuestionObserver {
	public function deleting(Question $question) {
		$question->answers()->delete();
	}
}