<?php

namespace App\Helpers;

class Column {

	private $name = "";
	private $label = "";

	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	public function setLabel(string $label) {
		$this->label = $label;
		return $this;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getLabel(): string {
		return $this->label;
	}

}
