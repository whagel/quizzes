<?php

namespace App\Helpers;

class ConfirmationModal {

	private $name = "";
	private $modal_title = "";

	public function __construct(string $name, string $modal_title) {
		$this->name = $name;
		$this->modal_title = $modal_title;
	}

	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	public function setModalTitle(string $modal_title) {
		$this->modal_title = $modal_title;
		return $this;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getModalTitle(): string {
		return $this->modal_title;
	}

}
