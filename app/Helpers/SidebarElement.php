<?php

namespace App\Helpers;

class SidebarElement {

	private $name = "";
	private $icon = "";
	private $route = "";

	public function setName(string $name) {
		$this->name = $name;
		return $this;
	}

	public function setIcon(string $icon) {
		$this->icon = $icon;
		return $this;
	}

	public function setRoute(string $route) {
		$this->route = $route;
		return $this;
	}

	public function getName(): string {
		return $this->name;
	}

	public function getIcon(): string {
		return $this->icon;
	}

	public function getRoute(): string {
		return $this->route;
	}

}
