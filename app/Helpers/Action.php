<?php

namespace App\Helpers;

use App\Helpers\ConfirmationModal;

class Action {

	private $btn_icon = "";
	private $btn_style = "";
	private $route = "";
	private $route_parameters = [];
	private $is_post_action = false;
	private $confirmation_modal;

	public function setBtnIcon(string $icon) {
		$this->btn_icon = $icon;
		return $this;
	}

	public function setBtnStyle(string $style) {
		$this->btn_style = $style;
		return $this;
	}

	public function setRoute(string $route, array $route_parameters = []) {
		$this->route = $route;
		$this->route_parameters = $route_parameters;
		return $this;
	}

	public function setPostAction() {
		$this->is_post_action = true;
		return $this;
	}

	public function setConfirmationModal(ConfirmationModal $modal) {
		$this->confirmation_modal = $modal;
		return $this;
	}

	public function getBtnIcon(): string {
		return $this->btn_icon;
	}

	public function getBtnStyle(): string {
		return $this->btn_style;
	}

	public function getRoute(): string {
		return $this->route;
	}

	public function isPostAction(): bool {
		return $this->is_post_action;
	}

	public function isConfirmationModalSet(): bool {
		return $this->confirmation_modal != null;
	}

	public function getConfirmationModal(): ConfirmationModal {
		return $this->confirmation_modal;
	}

	public function getModalName($id): string {
		return $this->confirmation_modal->getName().$id."Modal";
	}

}
