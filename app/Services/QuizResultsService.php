<?php

namespace App\Services;

use Illuminate\Http\Request;

use App\Models\Quiz;

class QuizResultsService {

	private $quiz;
	private $user_answers;
	private $correct_answers;

	public function __construct(Request $request) {
		$this->quiz = Quiz::where("id", $request->quiz)->with("comments.user", "questions.answers")->first();
		$this->user_answers = $request->except("_token", "quiz");
		$this->correct_answers = $this->quiz->getQuestionsWithCorrectAnswers();
		$this->setResults();
		$this->setQuestionClasses();
	}

	public function getQuizWithResults(): Quiz {
		return $this->quiz;
	}

	private function setResults() {
		$this->quiz->correct_answers = $this->countCorrectlyAnswered();
		$this->quiz->percentage_result = $this->countPercentageResult();
	}

	private function setQuestionClasses() {
		foreach($this->quiz->questions as $question) {
			
			$question->class = "wrong";
			$this->setAnswerClasses($question);

			if(!$this->questionAnswered($question->id)) continue;
			if($this->correct_answers[$question->id] == $this->user_answers[$question->id]) 
				$question->class = "correct";
		}
	}

	private function countCorrectlyAnswered() {
		$correctly_answered_amount = 0;
		foreach($this->quiz->questions as $key => $question) {
			if(!$this->questionAnswered($question->id)) continue;

			if($this->correct_answers[$question->id] == $this->user_answers[$question->id]) 
				$correctly_answered_amount++;	
		}
		return $correctly_answered_amount;
	}

	private function countPercentageResult() {
		$percentage = ($this->quiz->correct_answers/$this->quiz->questions->count()) * 100;
		return round($percentage)."%";
	}

	private function setAnswerClasses($question) {
		foreach($question->answers as $answer) {
			if($answer->is_correct) {
				$answer->class = "btn-success";
				continue;
			}
			if(isset($this->user_answers[$question->id]) && in_array($answer->id, $this->user_answers[$question->id])) {
				$answer->class = "btn-danger";
			}
		}
	}

	private function questionAnswered($id) {
		return isset($this->user_answers[$id]);
	}
	
}
