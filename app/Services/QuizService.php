<?php

namespace App\Services;

use Auth;
use Image;
use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Models\Quiz;
use App\Models\Question;
use App\Models\Answer;
use App\Models\QuizImage;

class QuizService {

	private $quiz;
	private $id;
	private $name;
	private $description;
	private $questions = [];
	private $image;

	public function __construct(Request $request) {
		$this->id = $request->id;
		$this->name = $request->name;
		$this->description = $request->description;
		$this->questions = json_decode($request->questions);
		if($request->has("quiz_image")) {
			$this->image = Image::make($request->quiz_image);
		}
	}

	public function storeQuiz() {
		$this->quiz = new Quiz();
		$this->quiz->name = $this->name;
		$this->quiz->description = $this->description;
		$this->quiz->user_id = Auth::id();
		$this->quiz->save();		
		$this->storeQuestions();
		if(isset($this->image)) {
			$this->storeImage();
		}
	}

	public function updateQuiz() {
		$this->quiz = Quiz::find($this->id);
		$this->quiz->name = $this->name;
		$this->quiz->description = $this->description;
		$this->quiz->save();	
		$this->quiz->questions()->delete();
		$this->storeQuestions();
		if(isset($this->image)) {
			$this->quiz->image()->delete();
			$this->storeImage();
		}
	}

	public function getQuizId(): int {
		return $this->quiz->id;
	}

	private function storeQuestions() {
		foreach($this->questions as $question) {
			$new_question = new Question();
			$new_question->content = $question->content;
			$this->quiz->questions()->save($new_question);
			$this->storeAnswers($question->answers, $new_question);
		}
	}

	private function storeAnswers(array $answers, Question $question) {
		$new_answers = [];
		foreach($answers as $answer) {
			$new_answers[] = array("question_id" => $question->id,
				"content" => $answer->content,
				"is_correct" => (bool)$answer->is_correct,
				"created_at" => Carbon::now(),
				"updated_at" => Carbon::now());
		}
		Answer::insert($new_answers);
	}

	private function storeImage() {
		$quiz_image = new QuizImage();
		$quiz_image->path = $this->generateImagePath();
		$this->quiz->image()->save($quiz_image);
		$this->image->fit(810, 300);
		$this->image->save($quiz_image->path);
	}

	private function generateImagePath(): string {
		$path = "";
		do {
			$path = "quiz_images/".str_random(10).".jpg";
		} while (QuizImage::where("path", $path)->first() instanceof QuizImage);
		return $path;
	}
}
