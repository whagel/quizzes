<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Models\Question;
use App\Interfaces\ViewableAdminTable;

class QuestionsTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "pytania";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("quiz_id")
				->setLabel("ID quizu"),
			(new Column())
				->setName("content")
				->setLabel("Pytanie"),
		];
	}

	public function getModel() {
		return Question::class;
	}

	public function getActions(): array {
		return [];
	}
}
