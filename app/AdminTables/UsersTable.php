<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Models\User;
use App\Helpers\ConfirmationModal;
use App\Interfaces\ViewableAdminTable;

class UsersTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "użytkownicy";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("name")
				->setLabel("Nazwa użytownika"),
			(new Column())
				->setName("email")
				->setLabel("E-mail"),
			(new Column())
				->setName("created_at")
				->setLabel("Zarejestrowano"),
		];
	}

	public function getModel() {
		return User::class;
	}

	public function getActions(): array {
		return [
			(new Action())
				->setBtnIcon("fa-ban")
				->setBtnStyle("danger")
				->setRoute("user.ban")
				->setPostAction()
				->setConfirmationModal(new ConfirmationModal("ban", "Czy na pewno chcesz zbanować użytkownika?"))
		];
	}
}
