<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Helpers\ConfirmationModal;
use App\Models\Quiz;
use App\Interfaces\ViewableAdminTable;

class QuizzesTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "quizy";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("user_id")
				->setLabel("ID użytkownika"),
			(new Column())
				->setName("name")
				->setLabel("Nazwa"),
			(new Column())
				->setName("description")
				->setLabel("Opis"),
			(new Column())
				->setName("created_at")
				->setLabel("Stworzono"),
		];
	}

	public function getModel() {
		return Quiz::class;
	}

	public function getActions(): array {
		return [
			(new Action())
				->setBtnIcon("fa-search")
				->setBtnStyle("info")
				->setRoute("quizzes.show"),
			(new Action())
				->setBtnIcon("fa-pencil")
				->setBtnStyle("success")
				->setRoute("quizzes.edit"),
			(new Action())
				->setBtnIcon("fa-trash")
				->setBtnStyle("danger")
				->setPostAction()
				->setRoute("quizzes.destroy")
				->setConfirmationModal(new ConfirmationModal("delete", "Czy na pewno chcesz usunąć ten quiz?")),
		];
	}
}
