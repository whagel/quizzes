<?php

namespace App\AdminTables;

use App\Helpers\SidebarElement;

abstract class AdminPanel {

	public function getSidebarElements(): array {
		return [
			(new SidebarElement)
				->setName("Quizy")
				->setIcon("fa-check-square-o")
				->setRoute("admin.panel"),
			(new SidebarElement)
				->setName("Pytania")
				->setIcon("fa-question")
				->setRoute("admin.questions"),
			(new SidebarElement)
				->setName("Odpowiedzi")
				->setIcon("fa-list")
				->setRoute("admin.answers"),
			(new SidebarElement)
				->setName("Użytkownicy")
				->setIcon("fa-users")
				->setRoute("admin.users"),
			(new SidebarElement)
				->setName("Komentarze")
				->setIcon("fa-comments")
				->setRoute("admin.comments"),
			(new SidebarElement)
				->setName("Oceny")
				->setIcon("fa-star-half-o")
				->setRoute("admin.ratings"),
			(new SidebarElement)
				->setName("Administratorzy")
				->setIcon("fa-lock")
				->setRoute("admin.admins"),
			(new SidebarElement)
				->setName("Bany")
				->setIcon("fa-ban")
				->setRoute("admin.bans")
		];
	}

	public function getData() {
		$columns = $this->getColumnNames();
		return $this->getModel()::select($columns)->paginate(30);
	}

	public function hasActions(): bool {
		$actions = $this->getActions();
		return sizeof($actions) != 0;
	}

	protected function getColumnNames(): array {
		$names = [];
		foreach($this->getColumns() as $column) {
			$names[] = $column->getName();
		}
		return $names;
	}

	abstract function getColumns(): array;
	abstract function getActions(): array;
	abstract function getModel();
}
