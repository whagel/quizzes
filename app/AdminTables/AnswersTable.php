<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Models\Answer;
use App\Interfaces\ViewableAdminTable;

class AnswersTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "odpowiedzi";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("question_id")
				->setLabel("ID pytania"),
			(new Column())
				->setName("content")
				->setLabel("Odpowiedź"),
			(new Column())
				->setName("is_correct")
				->setLabel("Jest poprawna?"),
		];
	}

	public function getModel() {
		return Answer::class;
	}

	public function getActions(): array {
		return [];
	}
}
