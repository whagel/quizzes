<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Models\Rating;
use App\Interfaces\ViewableAdminTable;

class RatingsTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "oceny";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("value")
				->setLabel("Ocena"),
			(new Column())
				->setName("user_id")
				->setLabel("ID użytkownika"),
		];
	}

	public function getModel() {
		return Rating::class;
	}

	public function getActions(): array {
		return [
			(new Action())
				->setBtnIcon("fa-trash")
				->setBtnStyle("danger")
				->setPostAction()
				->setRoute("ratings.destroy"),
		];
	}
}
