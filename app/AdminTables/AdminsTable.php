<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Models\Admin;
use App\Interfaces\ViewableAdminTable;

class AdminsTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "administratorzy";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("user_id")
				->setLabel("ID użytkownika"),
		];
	}

	public function getModel() {
		return Admin::class;
	}

	public function getActions(): array {
		return [];
	}
}
