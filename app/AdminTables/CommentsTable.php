<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Helpers\ConfirmationModal;
use App\Models\Comment;
use App\Interfaces\ViewableAdminTable;

class CommentsTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "komentarze";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("quiz_id")
				->setLabel("ID quizu"),
			(new Column())
				->setName("user_id")
				->setLabel("ID użytkownika"),
			(new Column())
				->setName("content")
				->setLabel("Komentarz"),
			(new Column())
				->setName("created_at")
				->setLabel("Stworzony"),
		];
	}

	public function getModel() {
		return Comment::class;
	}

	public function getActions(): array {
		return [
			(new Action())
				->setBtnIcon("fa-trash")
				->setBtnStyle("danger")
				->setPostAction()
				->setRoute("comments.destroy")
				->setConfirmationModal(new ConfirmationModal("delete", "Czy na pewno chcesz usunąć ten komentarz?"))
			];
	}
}
