<?php

namespace App\AdminTables;

use App\Helpers\Action;
use App\Helpers\Column;
use App\Models\UserBan;
use App\Interfaces\ViewableAdminTable;

class BansTable extends AdminPanel implements ViewableAdminTable {

	public function getTitle(): string {
		return "bany";
	}

	public function getColumns(): array {
		return [
			(new Column())
				->setName("id")
				->setLabel("ID"),
			(new Column())
				->setName("user_id")
				->setLabel("ID użytkownika"),
		];
	}

	public function getModel() {
		return UserBan::class;
	}

	public function getActions(): array {
		return [
			(new Action())
				->setBtnIcon("fa-trash")
				->setBtnStyle("danger")
				->setPostAction()
				->setRoute("bans.destroy"),
		];
	}
}
