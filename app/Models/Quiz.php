<?php

namespace App\Models;

use Auth;
use Illuminate\Database\Eloquent\Model;

use App\QuizImage;

class Quiz extends Model {

	public function user() {
		return $this->belongsTo("App\Models\User");
	}

	public function questions() {
		return $this->hasMany("App\Models\Question");
	}

	public function comments() {
		return $this->hasMany("App\Models\Comment");
	}

	public function image() {
		return $this->hasOne("App\Models\QuizImage");
	}

	public function ratings() {
		return $this->hasMany("App\Models\Rating");
	}

	public function getImage() {
		if($this->image == null) {
			return "quiz_images/default.jpg";
		}
		return $this->image->path;
	}

	public function getQuestionsWithCorrectAnswers() {
		$correct_answers = [];

		foreach($this->questions as $question) {
			$correct_answers[$question->id] = $question->answers->where("is_correct", true)->pluck("id")->toArray();
		}
		return $correct_answers;
	}

	public function canEdit(): bool {

		if(!Auth::check()) {
			return false;
		}

		if(Auth::user()->isAdmin()) {
			return true;
		}

		if(Auth::id() == $this->user_id) {
			return true;
		}

		return false;
	}

	public function getAvgRating(): float {
		$ratings = $this->ratings->pluck("value");
		if($ratings->count() == 0) {
			return 0;
		}
		return round($ratings->avg(), 2);
	}

	public function alreadyRated(): bool {
		$rating = $this->ratings->where("user_id", Auth::id());
		if($rating->count() == 0) {
			return false;
		}
		return true;
	}
}
