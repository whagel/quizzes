<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class QuizImage extends Model {
	
	public function quiz() {
		return $this->belongsTo("App\Models\Quiz");
	}
}
