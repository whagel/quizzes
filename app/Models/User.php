<?php

namespace App\Models;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable {
	
	use Notifiable;

	protected $fillable = [
		'name', 'email', 'password',
	];

	protected $hidden = [
		'password', 'remember_token',
	];

	public function quizzes() {
		return $this->hasMany("App\Models\User");
	}
	
	public function comments() {
		return $this->hasMany("App\Models\Comment");
	}

	public function admin() {
		return $this->hasOne("App\Models\Admin");
	}

	public function ban() {
		return $this->hasOne("App\Models\UserBan");
	}

	public function ratings() {
		return $this->hasMany("App\Models\Rating");
	}
	
	public function isAdmin() {
		return $this->admin != null;
	}

	public function isBanned() {
		return $this->ban != null;
	}
}
