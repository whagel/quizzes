<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Question extends Model {
	
	public function quiz() {
		return $this->belongsTo("App\Models\Quiz");
	}

	public function answers() {
		return $this->hasMany("App\Models\Answer");
	}

	public function getCorrectAnswers(): string {
		$correct_answers = $this->answers->where("is_correct", 1)->count();
		if($correct_answers <= 1) {
			return "";
		} 
		return "( poprawnych odpowiedzi: ".$correct_answers." )";
	}
}
