<?php

namespace App\Http\Middleware;

use Session;
use Auth;
use Closure;

use App\Models\User;

class CheckIfNotBanned {

    public function handle($request, Closure $next) {
        if (Auth::check() && Auth::user()->isBanned()) {
            $request->session()->flash("errors", collect("Wykonywanie tej akcji przez Ciebie zostało zablokowane przez administratora"));
            return redirect()->route("index");
        }
        return $next($request);
    }
}
