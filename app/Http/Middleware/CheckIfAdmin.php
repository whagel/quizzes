<?php

namespace App\Http\Middleware;

use Auth;
use Closure;

use App\Models\User;

class CheckIfAdmin {

	public function handle($request, Closure $next) {
		if (!Auth::check() || !Auth::user()->isAdmin()) {
            return redirect()->route("login");
        }
		return $next($request);
	}
}
