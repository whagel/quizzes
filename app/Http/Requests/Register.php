<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Register extends FormRequest {

	public function authorize() {
		return true;
	}

	public function rules() {
		return [
			"name" => "required|min:5|max:50",
			"email" => "required|email",
			"password" => "required|min:6|confirmed",
		];
	}

	public function messages() {
		return [
			"name.required" => "Nazwa użytkownika jest wymagana",
			"name.min" => "Nazwa użytkownika musi mieć co najmniej 5 znaków",
			"name.max" => "Nazwa użytkownika musi mieć co najwyżej 50 znaków",
			"email.required" => "E-mail jest wymagany",
			"email.email"  => "E-mail jest nieprawidłowy",
			"password.required"  => "Hasło jest wymagane",
			"password.min"  => "Hasło musi mieć co najmniej 6 znaków",
			"password.confirmed"  => "Hasła nie zgadzają się",
		];
	}
}
