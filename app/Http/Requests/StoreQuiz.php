<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreQuiz extends FormRequest {

	public function authorize() {
		return true;
	}

	public function rules() {
		return [
			"name" => "required|max:55",
			"description" => "required|max:255",
			"quiz_image" => "mimes:jpeg,gif,png|max:200000",
		];
	}

	public function messages() {
		return [
			"name.required" => "Nazwa quizu jest wymagana",
			"description.required"  => "Opis quizu jest wymagany",
			"name.max"  => "Nazwa quizu jest za długa!",
			"description.max"  => "Opis quizu jest za długi!",
			"quiz_image.mimes"  => "Nieprawidłowy format obrazka quizu",
			"quiz_image.size"  => "Obrazek quizu nie może być większy niż 20MB",
		];
	}
}
