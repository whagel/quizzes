<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class Login extends FormRequest {

	public function authorize() {
		return true;
	}

	public function rules() {
		return [
			"email" => "required|email",
			"password" => "required",
		];
	}

	public function messages() {
		return [
			"email.required" => "E-mail jest wymagany",
			"email.email"  => "E-mail jest nieprawidłowy",
			"password.required"  => "Hasło jest wymagane",
		];
	}
}
