<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreComment extends FormRequest {

	public function authorize() {
		return true;
	}

	public function rules() {
		return [
			"content" => "required|min:5|max:250",
		];
	}

	public function messages() {
		return [
			"content.required" => "Treść komentarza jest wymagana",
			"content.min" => "Komentarz musi mieć co najmniej 5 znaków",
			"content.max" => "Komentarz musi mieć co najwyżej 250 znaków",
		];
	}
}
