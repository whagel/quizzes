<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\UserBan;

class UserBanController extends Controller {

	public function deleteBan(Request $request) {
		$ban = UserBan::where("id", $request->id);
		$ban->delete();
		$request->session()->flash("success", "Użytkownik odbanowany pomyślnie!");
		return redirect()->back();
	}
	
}
