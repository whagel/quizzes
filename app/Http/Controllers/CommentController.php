<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use Auth;
use App\Http\Requests\StoreComment;
use App\Models\Comment;

class CommentController extends Controller {

	public function storeComment(StoreComment $request) {
		$comment = new Comment();
		$comment->quiz_id = $request->quiz_id;
		$comment->content = $request->content;

		$comment->user_id = Auth::id();
		$comment->save();
		$request->session()->flash("success", "Dodano komentarz.");
		
		return redirect()->route("quizzes.show", $request->quiz_id);
	}

	public function deleteComment(Request $request) {
		$comment = Comment::where("id", $request->id);
		$comment->delete();
		$request->session()->flash("success", "Komentarz usunięty pomyślnie!");
		return redirect()->back();
	}

}
