<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use Auth;
use App\Http\Requests\StoreRating;
use App\Models\Quiz;
use App\Models\Rating;

class RatingController extends Controller {

	public function storeRating(StoreRating $request) {

		$user_id =  Auth::id();
		$quiz = Quiz::where("id", $request->quiz_id)->first();
		if($quiz->alreadyRated()) {
			$quiz->ratings->where("user_id", $user_id)->first()->delete();
		}

		$rating = new Rating();
		$rating->quiz_id = $request->quiz_id;
		$rating->user_id = $user_id;
		$rating->value = $request->value;
		$rating->save();
		$request->session()->flash("success", "Dodano ocenę.");
		
		return redirect()->route("quizzes.show", $request->quiz_id);
	}

	public function deleteRating(Request $request) {
		$rating = Rating::where("id", $request->id);
		$rating->delete();
		$request->session()->flash("success", "Ocena usunięta pomyślnie!");
		return redirect()->back();
	}
	
}
