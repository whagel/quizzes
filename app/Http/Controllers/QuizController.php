<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Carbon\Carbon;

use App\Http\Requests\StoreQuiz;
use App\Services\QuizResultsService;
use App\Services\QuizService;
use App\Models\Quiz;
use App\Models\Question;
use App\Models\Answers;

class QuizController extends Controller {

	protected function getIndexView() {
		$quizzes = Quiz::with("image", "ratings", "user");
		$quizzes = $quizzes->get()->sortByDesc(function($quiz) { return $quiz->getAvgRating(); });
		$last_month_best_quizzes = $quizzes->where("created_at", ">=", Carbon::now()->subMonth()); 
		$top_5_quizzes = $quizzes->take(5);
		return view("index")->with("quizzes", $last_month_best_quizzes)->with("top_5_quizzes", $top_5_quizzes);
	}

	public function getTopQuizzesView() {
		$quizzes = Quiz::with("image", "ratings", "user");
		$quizzes = $quizzes->get()->sortByDesc(function($quiz) { return $quiz->getAvgRating(); });
		return view("quizzes")->with("quizzes", $quizzes);
	}

	public function getNewestQuizzesView() {
		$quizzes = Quiz::orderByDesc("created_at")->with("image")->get();
		return view("quizzes")->with("quizzes", $quizzes);
	}

	public function getQuizView(Request $request) {
		$id = $request->id;
		$quiz = Quiz::where("id", $id)->with("comments.user", "questions.answers")->first();

		if($quiz == null) {
			$request->session()->flash("errors", collect("Nie ma takiego quizu!"));
			return redirect()->route("index");
		}

		return view("quiz.container")->with("results", false)->with("quiz", $quiz);
	}

	public function getQuizGeneratorView() {
		return view("generator.container");
	}

	public function getEditQuizView(Request $request) {
		$id = $request->id;
		$quiz = Quiz::where("id", $id)->with("questions.answers")->first();

		if($quiz == null) {
			$request->session()->flash("errors", collect("Nie ma takiego quizu!"));
			return redirect()->route("index");
		}
		
		if(!$quiz->canEdit()) {
			$request->session()->flash("errors", collect("Nie możesz edytować tego quizu!"));
			return redirect()->route("index");
		}

		return view("generator.container")->with("quiz", $quiz);
	}

	public function updateQuiz(StoreQuiz $request) {
		$quiz_service = new QuizService($request);
		$quiz_service->updateQuiz();
		$request->session()->flash("success", "Quiz edytowany pomyślnie!");
		return $quiz_service->getQuizId();
	}

	public function deleteQuiz(Request $request) {
		$quiz = Quiz::where("id", $request->id);
		$quiz->delete();
		$request->session()->flash("success", "Quiz usunięty pomyślnie!");
		return redirect()->back();
	}

	public function storeQuiz(StoreQuiz $request) {
		$quiz_service = new QuizService($request);
		$quiz_service->storeQuiz();
		$request->session()->flash("success", "Quiz opublikowany pomyślnie!");
		return $quiz_service->getQuizId();
	}

	public function getQuizResults(Request $request) {
		$quiz_results = new QuizResultsService($request);
		$quiz = $quiz_results->getQuizWithResults();
		return view("quiz.container")->with("results", true)->with("quiz", $quiz);
	}

}
