<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\AdminTables\QuizzesTable;
use App\AdminTables\QuestionsTable;
use App\AdminTables\AnswersTable;
use App\AdminTables\UsersTable;
use App\AdminTables\CommentsTable;
use App\AdminTables\RatingsTable;
use App\AdminTables\AdminsTable;
use App\AdminTables\BansTable;

class AdminPanelController extends Controller {

	public function getAdminPanelView() {
		return view("admin.panel")->with("admin_table", new QuizzesTable());
	}

	public function getQuestionsTableView() {
		return view("admin.panel")->with("admin_table", new QuestionsTable());
	}

	public function getAnswersTableView() {
		return view("admin.panel")->with("admin_table", new AnswersTable());
	}

	public function getUsersTableView() {
		return view("admin.panel")->with("admin_table", new UsersTable());
	}

	public function getCommentsTableView() {
		return view("admin.panel")->with("admin_table", new CommentsTable());
	}

	public function getRatingsTableView() {
		return view("admin.panel")->with("admin_table", new RatingsTable());
	}

	public function getAdminsTableView() {
		return view("admin.panel")->with("admin_table", new AdminsTable());
	}

	public function getBansTableView() {
		return view("admin.panel")->with("admin_table", new BansTable());
	}

}
