<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\User;
use App\Models\UserBan;

class UserController extends Controller {

	public function banUser(Request $request) {
		$user = User::where("id", $request->id)->first();
		$user->ban()->save(new UserBan());
		$request->session()->flash("success", "Pomyślnie zbanowano użytkownika!");
		return redirect()->back();
	}
	
}
