<?php

namespace App\Http\Controllers;

use Session;
use Auth;
use Illuminate\Http\Request;

use App\Models\User;
use App\Http\Requests\Login;
use App\Http\Requests\Register;
use Illuminate\Support\Facades\Hash;

class AuthController extends Controller {

	public function getLoginView() {
		return view("auth.login");
	}

	public function login(Login $request) {
		$login_data = [
			"email" => $request->email,
			"password" => $request->password
		];

		if(Auth::attempt($login_data)) {
			return redirect()->route("index");
		} else {
			Session::flash("errors", collect("Nieprawidłowy e-mail lub hasło")); 
			return redirect()->route("login");
		}
	}

	public function logout() {
		Auth::logout();
		Session::flash("success", "Wylogowano pomyślnie"); 
		return redirect()->route("index");
	}

	public function getRegisterView() {
		return view("auth.register");
	}

	public function register(Register $request) {
		$user = new User();
		$user->name = $request->name;
		$user->email = $request->email;
		$user->password = Hash::make($request->password);
		$user->save();
		
		Session::flash("success", "Konto stworzone pomyślnie! Możesz się teraz zalogować."); 
		return redirect()->route("login");
	}

		public function storeQuizRating(Request $request) {
		$rating = $request->input("rating");
	}
	
}
