<?php

namespace App\Interfaces;

interface ViewableAdminTable {

	public function getSidebarElements(): array;
	public function getTitle(): string;
	public function getColumns(): array;
	public function getData();
 	public function getModel();
}