<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Models\Quiz;
use App\Models\Question;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        \Carbon\Carbon::setLocale("pl");
        Quiz::observe("App\\Observers\\QuizObserver");
        Question::observe("App\\Observers\\QuestionObserver");
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
